**Project brief description**

The project aims to develop a software system for the management of a cinema.
For simplicity, we assume that the cinema has fixed timeslots from 9am-9pm (each time slot lasts exactly 2 hours) and the manager can assign the timeslots to each movie.

For customer side: A mobile app (Android) that  customers can use to view availability and book sessions
For business side: A web application the staff could use the web to make face to face bookings and payment
the manager can perform selected administrative functions (calculating income for the week, reviewing aspects of the business, delete movies that no longer be screened, add new movie, assign time slots to each movie based on its popularity)

**User Manual**

**Mobile App**

**Customer**
Create a new account
Sign In
All movies screening are displayed
When a particular movie is selected it shows the details of that movie.
when 'Book tickets' is clicked it shows us the different dates and screening time.
select and one slot then it gives us three categories to book a ticket which are-child,adult and senior.
Then the seating map is displayed and the user can select the seat they want to sit in.
The payment is done as a card transaction as card details are asked to be entered.
A QR code is generated


**Web Application**

**Manager**
log in with a manager account (you can create a manager account from the login page)
In 'View all movies' all movies screening are displayed
When a movie is selected it shows all the details of that movie
In 'Add movies' new movies can be added with the timeslot of when they are screening (at least one timeslot should exist to add a movie)
In 'Manage timeslots' new timeslots can be added (timeslots are not always updated properly if the application is running on localhost. Relaunch the application to ensure new timeslots are added).
In 'Delete movies' movies can be removed from the application (subject to infrequent errors)
In 'Statistics' it shows Three graphs
In 'Graph-Income per Day' it shows a line graph of how much income from all the movies combined in a day is
In 'Graph-Income per movie' it shows a bar graph which compares the income of each movie
In 'Graph-In time Period' it shows a bar graph which compares the number of tickets sold per day
If we enter two dates then click 'create graph' it shows us a bar graph which compares the number of tickets sold of those two days
Staff

log in with a staff account (you can create a staff account from the login page)
In 'View all movies' all movies screening are displayed
When a movie is selected it shows all the details of that movie and gives an option to book tickets for a specific timeslot.
When 'Book ticket' is clicked it gives us three categories to book a ticket which are-child,adult and senior.
When 'Choose seat' is selected it shows the seating map and the user can select the seat they want to sit in. Seats can be reselected if the user changes their mind.
Once the seats are selected properly, two payment options, cash and card will be available.
when 'pay by card' is selected it shows the total amount that needs to be paid and it asks for the cards pin number.
After that is confirmed it shows payment successful and gives an option to print a ticket.
when 'print ticket' is selected the ticket is generated as a pdf.
all tickets are saved and can be viewed in the "View tickets" section.
When 'pay by cash' is selected it shows the total amount that needs to be paid and it asks for the amount the customer has paid.
After that is confirmed it shows payment successful and gives an option to print a ticket.
when 'print ticket' is selected the ticket is generated as a pdf.
all tickets are saved and can be viewed in the "View tickets" section.
The final version of web is on the master branch in the Flask folder. The final version of mobile can be found on the mobile_app_18/4 branch (excludes Flask folder).
