package com.example.cinema;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ChooseSeat extends AppCompatActivity {
    Button a1;
    Button a2;
    Button a3;
    Button a4;
    Button a5;
    Button a6;
    Button a7;
    Button a8;
    Button a9;
    Button a10;
    Button b1;
    Button b2;
    Button b3;
    Button b4;
    Button b5;
    Button b6;
    Button b7;
    Button b8;
    Button b9;
    Button b10;
    Button c1;
    Button c2;
    Button c3;
    Button c4;
    Button c5;
    Button c6;
    Button c7;
    Button c8;
    Button c9;
    Button c10;

    String userName;
    String movieCode;
    int counterInt;
    int slotOpted;
    int cnt=0;

    public void bookSeat(View view){
        if (cnt<=counterInt){
            switch (view.getId()){
                case R.id.aOne :
                    a1.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aTwo :
                    a2.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aThree :
                    a3.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aFour :
                    a4.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aFive :
                    a5.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aSix :
                    a6.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aSeven :
                    a7.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aEight :
                    a8.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aNine :
                    a9.setAlpha(0);
                    cnt++;
                    break;
                case R.id.aTen :
                    a10.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bOne :
                    b1.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bTwo :
                    b2.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bThree :
                    b3.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bFour :
                    b4.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bFive :
                    b5.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bSix :
                    b6.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bSeven :
                    b7.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bEight :
                    b8.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bNine :
                    b9.setAlpha(0);
                    cnt++;
                    break;
                case R.id.bTen :
                    b10.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cOne :
                    c1.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cTwo :
                    c2.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cThree :
                    c3.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cFour :
                    c4.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cFive :
                    c5.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cSix :
                    c6.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cSeven :
                    c7.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cEight :
                    c8.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cNine :
                    c9.setAlpha(0);
                    cnt++;
                    break;
                case R.id.cTen :
                    c10.setAlpha(0);
                    cnt++;
                    break;

                default:break;
            }
        }
    }
    public void goToPayment(View view){
        Intent intent = new Intent(getApplicationContext(), Payment.class);
        intent.putExtra("seats", counterInt);
        intent.putExtra("timeSlot", slotOpted);
        intent.putExtra("movieId", movieCode);
        intent.putExtra("userName", userName);
        // Toast.makeText(this, userName, Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_seat);
        Intent intent = getIntent();
        userName = intent.getStringExtra("userName");
        movieCode = intent.getStringExtra("movieId");
        counterInt = intent.getIntExtra("seats",1);
        slotOpted = intent.getIntExtra("timeSLot",1);
        a1 = (Button) findViewById(R.id.aOne);
        a2 = (Button) findViewById(R.id.aTwo);
        a3 = (Button) findViewById(R.id.aThree);
        a4 = (Button) findViewById(R.id.aFour);
        a5 = (Button) findViewById(R.id.aFive);
        a6 = (Button) findViewById(R.id.aSix);
        a7 = (Button) findViewById(R.id.aSeven);
        a8 = (Button) findViewById(R.id.aEight);
        a9 = (Button) findViewById(R.id.aNine);
        a10 = (Button) findViewById(R.id.aTen);

        b1 = (Button) findViewById(R.id.bOne);
        b2 = (Button) findViewById(R.id.bTwo);
        b3 = (Button) findViewById(R.id.bThree);
        b4 = (Button) findViewById(R.id.bFour);
        b5 = (Button) findViewById(R.id.bFive);
        b6 = (Button) findViewById(R.id.bSix);
        b7 = (Button) findViewById(R.id.bSeven);
        b8 = (Button) findViewById(R.id.bEight);
        b9 = (Button) findViewById(R.id.bNine);
        b10 = (Button) findViewById(R.id.bTen);

        c1 = (Button) findViewById(R.id.cOne);
        c2 = (Button) findViewById(R.id.cTwo);
        c3 = (Button) findViewById(R.id.cThree);
        c4 = (Button) findViewById(R.id.cFour);
        c5 = (Button) findViewById(R.id.cFive);
        c6 = (Button) findViewById(R.id.cSix);
        c7 = (Button) findViewById(R.id.cSeven);
        c8 = (Button) findViewById(R.id.cEight);
        c9 = (Button) findViewById(R.id.cNine);
        c10 = (Button) findViewById(R.id.cTen);
    }
}