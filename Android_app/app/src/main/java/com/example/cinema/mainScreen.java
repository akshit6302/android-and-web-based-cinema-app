package com.example.cinema;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class mainScreen extends AppCompatActivity {
   Intent intent;
   String userName;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case (R.id.wallet):{
                Toast.makeText(this, "Wallet Clicked!", Toast.LENGTH_SHORT).show();
                return true;
            }
            case (R.id.settings):{
                Toast.makeText(this, "Settings Clicked!", Toast.LENGTH_SHORT).show();
                return true;
            }
            case (R.id.logOut):{
                Toast.makeText(this, "LogOut Clicked!", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            }
            default: return true;
        }
    }

    public void movieDetails(View view){
        Intent intentb = new Intent(getApplicationContext(),posterDetail.class);
        int id = view.getId();
        switch (view.getId()){
            case R.id.m1 : {
                intentb.putExtra("id", R.drawable.jr);
                intentb.putExtra("title", "Joker Rick");
                intentb.putExtra("movieId", "JR");
                intentb.putExtra("userName", userName);
                startActivity(intentb);
                break;
            }
            case R.id.m2 : {
                intentb.putExtra("id", R.drawable.rm);
                intentb.putExtra("title", "Rick and Morty: Dark");
                intentb.putExtra("movieId", "RnM");
                intentb.putExtra("userName", userName);
                startActivity(intentb);
                break;
            }
            case R.id.m3 : {
                intentb.putExtra("id", R.drawable.n);
                intentb.putExtra("title", "Naruto");
                intentb.putExtra("movieId", "NS");
                intentb.putExtra("userName", userName);
                startActivity(intentb);
                break;
            }
            case R.id.m4 : {
                intentb.putExtra("id", R.drawable.e1);
                intentb.putExtra("title", "Existential Crisis");
                intentb.putExtra("movieId", "EC");
                intentb.putExtra("userName", userName);
                startActivity(intentb);
                break;
            }
            default: break;
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        intent = getIntent();
        userName=intent.getStringExtra("userName");
        //Toast.makeText(this, userName, Toast.LENGTH_SHORT).show();




    }
}