package com.example.cinema;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class posterDetail extends AppCompatActivity {
    String movieCode;
    String userName;
    public void bookTickets(View view){
        Intent intent = new Intent(getApplicationContext(),BookTickets.class);
        intent.putExtra("movieId", movieCode);
        intent.putExtra("userName", userName);
        startActivity(intent);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poster_detail);
        TextView textView= (TextView)findViewById(R.id.textView);
        TextView type= (TextView)findViewById(R.id.info);
        TextView actors= (TextView)findViewById(R.id.stars);
        ImageView image = (ImageView) findViewById(R.id.imageView);
        Bundle bundle = getIntent().getExtras();
        Intent intent = getIntent();
        userName = intent.getStringExtra("userName");
      //  Toast.makeText(this, userName, Toast.LENGTH_SHORT).show();
        movieCode = intent.getStringExtra("movieId");
        //Toast.makeText(this, "Mcd is: "+ movieCode, Toast.LENGTH_SHORT).show();
        String title = intent.getStringExtra("title");
        if (bundle!=null){
            int resId = bundle.getInt("id");
            image.setImageResource(resId);
        }
        textView.setText(title);
        textView.setTextSize(40);
        if (title.equals("Joker Rick")){
            type.setText("Animation, Action, Comedy, Adult");
            actors.setText("An animated series on adult-swim about the infinite adventures of Rick, " +
                    "a genius alcoholic and careless scientist, with his grandson Morty, " +
                    "a 14 year-old anxious boy who is not so smart. " +
                    "Together, they explore the infinite universes; " +
                    "causing mayhem and running into trouble.");
            actors.setTextSize(20);
            type.setTextSize(20);
        }
        else if(title.equals("Rick and Morty: Dark")){
            type.setText("Animation, Action, Comedy, Adult, Dark");
            actors.setText("An animated series on adult-swim about the infinite adventures of Rick, " +
                    "a genius alcoholic and careless scientist, with his grandson Morty, " +
                    "a 14 year-old anxious boy who is not so smart. " +
                    "Together, they explore the infinite universes; " +
                    "causing mayhem and running into trouble.");
            actors.setTextSize(20);
            type.setTextSize(20);
        }
        else if (title.equals("Naruto")) {
            type.setText("Anime, Action, Comedy");
            actors.setText("Naruto: Shippuden is the continuation of the original animated TV series Naruto." +
                    "The story revolves around an older and slightly more matured Uzumaki Naruto and his quest " +
                    "to save his friend Uchiha Sasuke from the grips of the snake-like Shinobi, Orochimaru. " +
                    "After 2 and a half years Naruto finally returns to his village of Konoha," +
                    " and sets about putting his ambitions to work, though it will not be easy, " +
                    "as he has amassed a few (more dangerous) enemies," +
                    " in the likes of the shinobi organization; Akatsuki.");
            actors.setTextSize(20);
            type.setTextSize(20);
        }
        else{
            type.setText("Animation, Action, Comedy, Adult, Sad");
            actors.setText("An animated series on adult-swim about the infinite adventures of Rick, " +
                    "a genius alcoholic and careless scientist, with his grandson Morty, " +
                    "a 14 year-old anxious boy who is not so smart. " +
                    "Together, they explore the infinite universes; " +
                    "causing mayhem and running into trouble.");
            actors.setTextSize(20);
            type.setTextSize(20);
        }


    }
}