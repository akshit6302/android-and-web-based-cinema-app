package com.example.cinema;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Payment extends AppCompatActivity {
    String movieCode;
    String userName;
    int seats=0, slot=0;
    TextView textView;
    TextView textView2;
    TextView pName;
    EditText pCard;
    TextView pDate;
    TextView pCvv;
    public void doPayment(View view){
        //Toast.makeText(this, "Thank you for the purchase.", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), Qr.class);
            intent.putExtra("movieId", movieCode);
            startActivity(intent);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        textView = findViewById(R.id.textView3);
        textView2 = findViewById(R.id.textView4);
        pName = findViewById(R.id.pName);
        pCard = (EditText) findViewById(R.id.pCard);
        pDate = findViewById(R.id.pDate);
        pCvv = findViewById(R.id.pCvv);

        Intent intent = getIntent();
        userName= intent.getStringExtra("userName");
        seats = intent.getIntExtra("seats", 1);
        slot = intent.getIntExtra("timeSlot", 1);
        movieCode = intent.getStringExtra("movieId");
       // Toast.makeText(this, "Mcd is: "+ movieCode, Toast.LENGTH_SHORT).show();
        textView.setText("Total Amount to pay: £"+ Integer.toString(seats*5));
        textView.setTextSize(30);
        if (slot==1){
            textView2.setText("Show Time: 11:00 am");
            textView2.setTextSize(30);
        }else if(slot==2){
            textView2.setText("Show Time: 4:00 pm");
            textView2.setTextSize(30);
        }else{
            textView2.setText("Show Time: 8:00 pm");
            textView2.setTextSize(30);
        }


    }
}