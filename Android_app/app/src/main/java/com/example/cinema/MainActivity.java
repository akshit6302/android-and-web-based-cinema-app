package com.example.cinema;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase cinema;
    Boolean loginStat;

    public void logIn(View view){
       EditText userName = (EditText) findViewById(R.id.emailText);
       String loginKey1 = userName.getText().toString();
       EditText password = (EditText) findViewById(R.id.passId);
       String loginKey2 = password.getText().toString();
        Cursor c = cinema.rawQuery("SELECT*  FROM dbase;", null);
        int out1 = c.getColumnIndex("name");
        int out2 = c.getColumnIndex("pass");
        c.moveToFirst();
        while(!c.isAfterLast()){
            String passTemp = c.getString(out2);
            String usernameTemp = c.getString(out1);
            Log.i("check data", usernameTemp +" :: "+ passTemp);

            if (loginKey1.equals(usernameTemp)&&loginKey2.equals(passTemp)){
                Toast.makeText(this, c.getString(out1), Toast.LENGTH_SHORT).show();
                Log.i("check Success", c.getString(out2));
                c.moveToLast();
                Intent intenta = new Intent(getApplicationContext(),mainScreen.class);
                intenta.putExtra("userName", usernameTemp);
                startActivity(intenta);
                loginStat=true;
                break;

            }else{
                c.moveToNext();
                loginStat=false;
            }
           }
        if (!loginStat){
            Toast.makeText(this, "Wrong UserName or Password.", Toast.LENGTH_SHORT).show();
        }
    }
    public void signUp(View view){
        Intent intent = new Intent(getApplicationContext(),signUp.class);
        startActivity(intent);
   }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cinema = openOrCreateDatabase("dbase", Context.MODE_PRIVATE, null);
        cinema.execSQL("CREATE TABLE IF NOT EXISTS dbase(name VARCHAR, email VARCHAR, pass VARCHAR);");

    }
}