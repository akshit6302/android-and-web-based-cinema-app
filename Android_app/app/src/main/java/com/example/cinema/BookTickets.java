package com.example.cinema;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class BookTickets extends AppCompatActivity {
    SQLiteDatabase sqLiteDatabase;
    String userName;
    String movieCode;
    int counterInt=0;
    int slotOpted=0;
    Button button1 ;
    Button button2 ;
    Button button3 ;
    Button button4;
    Button button5;
    Button button6;
    TextView counter;
    TextView head;
    TextView title;
    DatePicker datePicker;
    public void timeSlot(View view){
        switch (view.getId()){
            case R.id.button1: {
                slotOpted =1;
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
            }
            case R.id.button2: {
                slotOpted = 2;
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
            }
            case R.id.button3: {
                slotOpted =3;
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
            }
            default: break;
        }
        button1.setAlpha(0);
        button2.setAlpha(0);
        button3.setAlpha(0);
        button4.setAlpha(1);
        button5.setAlpha(1);
        button6.setAlpha(1);
        head.setAlpha(0);
        counter.setAlpha(1);
        title.setAlpha(1);

    }
    @SuppressLint("SetTextI18n")
    public void plusOrMinus(View view){
        switch (view.getId()){
            case R.id.minus: {
                if (counterInt>0){
                    counterInt= counterInt - 1;
                }
                break;
            }
            case R.id.plus: {
                counterInt++;
                break;
            }
            default: break;
        }
        counter.setText(Integer.toString(counterInt));
    }
    public void goToPayment(View view){
        Intent intent = new Intent(getApplicationContext(), ChooseSeat.class);
        intent.putExtra("seats", counterInt);
        intent.putExtra("timeSlot", slotOpted);
        intent.putExtra("movieId", movieCode);
        intent.putExtra("userName", userName);
       // Toast.makeText(this, userName, Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_tickets);
        sqLiteDatabase = openOrCreateDatabase("movieDb", Context.MODE_PRIVATE, null);
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS dbase(name VARCHAR, movieId VARCHAR, date DATE, seats VARCHAR, slot INT);");
        //children ticket and adult ticket
        //

        Intent intent = getIntent();
        userName = intent.getStringExtra("userName");
        Toast.makeText(this, userName, Toast.LENGTH_SHORT).show();
        movieCode = intent.getStringExtra("movieId");
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4= findViewById(R.id.minus);
        button5 = findViewById(R.id.plus);
        counter = findViewById(R.id.counter);
        head= findViewById(R.id.textView2);
        head.setTextSize(30);
        title = findViewById(R.id.textView4);
        button6 = findViewById(R.id.payment);
        datePicker = (DatePicker) findViewById(R.id.simpleDatePicker);




    }
}