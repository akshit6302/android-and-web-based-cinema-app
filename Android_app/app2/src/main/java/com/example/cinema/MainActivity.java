package com.example.cinema;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

   public void logIn(View view){
       //Toast.makeText(this, "Welcome Back User!", Toast.LENGTH_SHORT).show();
       Intent intenta = new Intent(getApplicationContext(),mainScreen.class);
       startActivity(intenta);
   }
    public void signUp(View view){
        Intent intent = new Intent(getApplicationContext(),signUp.class);
        startActivity(intent);
   }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}