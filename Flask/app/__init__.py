from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import datetime
import logging

from flask_admin import Admin

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app);

admin = Admin(app,template_mode='bootstrap3')

#handles all migrateion
migrate = Migrate(app, db, render_as_batch=True)
logging.basicConfig(filename = 'logz', filemode='w', format='%(levelname)s:%(message)s')

from app import views, models
