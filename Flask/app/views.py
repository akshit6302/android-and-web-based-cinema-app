from flask import render_template, request, url_for, flash, redirect, g, make_response, session
from app import app, db, admin, models
from flask_admin.contrib.sqla import ModelView
from app.forms import MovieForm, SearchForm, LoginForm, RegisterForm, TimeslotForm, TicketForm, CashForm, PayForm, DateForm
from app.models import Movie, Timeslot, Order_Ticket, Seats, StaffAccount, Tick
from flask_weasyprint import render_pdf, HTML

import datetime
import logging
import string
from sqlalchemy.sql import func
from datetime import datetime
'import pdfkit'

admin.add_view(ModelView(Movie, db.session))
admin.add_view(ModelView(Timeslot, db.session))
admin.add_view(ModelView(Order_Ticket, db.session))
admin.add_view(ModelView(Tick, db.session))
admin.add_view(ModelView(Seats, db.session))
admin.add_view(ModelView(StaffAccount, db.session))

mode = -10

@app.route('/', methods=['GET','POST'])
def login():
    app.logger.debug('Successfully redirected to login route')
    session.pop('key', None)
    form = LoginForm()
    if form.validate_on_submit():
        for m in models.StaffAccount.query.all():
            if m.username == form.username.data and m.password == form.password.data:
                app.logger.debug('Found matching details')
                session['key'] = m.id
                global mode
                if m.admin == True:
                    app.logger.debug('Business owner login performed')
                    mode = 20
                elif m.admin == False:
                    app.logger.debug('staff login performed')
                    mode = 10
                return redirect( url_for( 'AllMovies' ))
        flash("Incorrect details")
    return render_template('login.html', form = form)


@app.route('/AddMovies', methods=['GET','POST'])
def AddMovies():
    if 'key' in session:
        app.logger.debug('Redirected to adding movies route')
        form = MovieForm()
        if form.validate_on_submit():
            file_name=form.file.data
            m = Movie(MovieCode=form.MovieCode.data,title=form.title.data,director=form.director.data,leadActors=form.leadActors.data,description=form.description.data,name=file_name.filename, data=file_name.read())
            for t_id in form.timeslot.data:
                t = Timeslot.query.get(t_id)
                m.timeslot.append(t)
            db.session.add(m)
            db.session.commit()
            db.session.refresh(m)
            app.logger.debug("new id: " + str(m.id))
            mid = m.id
            # add seat
            movie = Movie.query.filter_by(id=mid).first()
            app.logger.debug("move" + str(mid))
            for t_id in form.timeslot.data:
                for i in range(3):
                    for j in range(10):
                        if i==0:
                            seat='A'+str(j+1)
                            s=Seats(movie_id=movie.id,timeslot_id=t_id,seat=seat,status=False, reserved=False, vip=True)
                            db.session.add(s)
                            db.session.commit()
                        if i==1:
                            seat='B'+str(j+1)
                            s= Seats(movie_id=movie.id,timeslot_id=t_id,seat=seat,status=False, reserved=False, vip=False)
                            db.session.add(s)
                            db.session.commit()
                        if i==2:
                            seat='C'+str(j+1)
                            s= Seats(movie_id=movie.id,timeslot_id=t_id,seat=seat,status=False, reserved=False, vip=False)
                            db.session.add(s)
                            db.session.commit()
            app.logger.debug('added new movie to the database & created 30 seats for each associate timeslot in the database')
            return redirect(url_for('AllMovies'))
        return render_template('addMovie.html',title = 'Add Movies', form = form, mode = mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))



@app.route('/allMovies', methods=['GET','POST'])
def AllMovies():
    if 'key' in session:
        form = SearchForm()
        m = Movie.query.all()
        dd = Movie.query.all()
        if form.validate_on_submit() and form.target.data != "":
            target = form.target.data
            for d in dd:
                if not (str(target).lower() in str(d.title).lower()):
                    if not (str(target).lower() in str(d.MovieCode).lower()):
                        if not (str(target).lower() in str(d.director).lower()):
                            if not (str(target).lower() in str(d.leadActors).lower()):
                                for times in d.timeslot:
                                    if (str(target).lower() in str(times.slot).lower()):
                                        return render_template('allMovies.html',title ='All Movies', movie = m, mode = mode, form = form)
                                m.remove(d)
        app.logger.debug('redirected to show a list of all movies')
        return render_template('allMovies.html',title ='All Movies', movie = m, mode = mode, form = form)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))



@app.route('/info', methods=['GET','POST'])
def info():
    if 'key' in session:
    	m=Movie.query.filter_by(id = int(request.args.get('id'))).first()
    	return render_template('info2.html',title ='info',m = m, mode = mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))


@app.route('/delete', methods=['GET','POST'])
def deleteList():
    if 'key' in session:
        form = SearchForm()
        m = Movie.query.all()
        dd = Movie.query.all()
        if form.validate_on_submit() and form.target.data != "":
            target = form.target.data
            for d in dd:
                if not (str(target).lower() in str(d.title).lower()):
                    if not (str(target).lower() in str(d.MovieCode).lower()):
                        if not (str(target).lower() in str(d.director).lower()):
                            if not (str(target).lower() in str(d.leadActors).lower()):
                                m.remove(d)
        return render_template('deleteList.html',title='All Movies', movie = m, mode = mode, form = form)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))


@app.route('/delete/<id>',methods=['GET','POST'])
def delete(id):
    if 'key' in session:
        d=Movie.query.filter_by(id=int(id)).first()
        db.session.delete(d)
        db.session.commit()
        return redirect(url_for('deleteList'))
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))



@app.route('/timeslot', methods=['GET','POST'])
def TimeSlot():
    if 'key' in session:
        app.logger.debug('Redirected to timeslot route')
        time = Timeslot.query.all()
        form = TimeslotForm()

        if form.validate_on_submit():
            '''
            for t in time:
                if t.slot == form.slot.data:
                    exist=1
                    t.delete = False
                    db.session.commit()
            if exist==0:
            '''
            t_new = Timeslot(slot=form.slot.data)
            db.session.add(t_new)
            db.session.commit()
            app.logger.debug('Added a new timeslot to the database')
            return redirect(url_for('TimeSlot'))
        return render_template('manageTimeslot.html',title='Timeslot',form=form, time=time, mode = mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))


@app.route('/bookticket', methods=['GET','POST'])
def bookticket():
    if 'key' in session:
        mid = request.args.get('mid')
        tid = request.args.get('tid')
        order = Order_Ticket.query.filter_by(movie_id=mid, timeslot_id=tid, date=func.date(datetime.today())).first()
        if order is None:
            order = Order_Ticket(movie_id=mid, timeslot_id=tid, date=datetime.today())
            db.session.add(order)
            db.session.commit()
            ss = Seats.query.all()
            for s in ss:
                s.status = False
                s.reserved = False
        form = TicketForm()
        if form.validate_on_submit():
            if ng(form.ticket_a.data, form.ticket_c.data, form.ticket_s.data) != 0:
                quant = t = c= a = s = 0
                if form.ticket_c.data != None:
                    quant += form.ticket_c.data
                    order.income += form.ticket_c.data * 6
                    c = int(form.ticket_c.data)
                    t += form.ticket_c.data * 6
                if form.ticket_a.data != None:
                    quant += form.ticket_a.data
                    order.income += form.ticket_a.data * 8
                    a = int(form.ticket_a.data)
                    t += form.ticket_a.data * 8
                if form.ticket_s.data != None:
                    quant += form.ticket_s.data
                    order.income += form.ticket_s.data * 7
                    s = int(form.ticket_s.data)
                    t += form.ticket_s.data * 7
                if (quant + order.quantity) <= 30:
                    if 'submit2' in request.form:
                        order.quantity += quant
                        db.session.commit()
                        mlist = str(t) + "," + str(c) + "," + str(a) + "," + str(s) + "," + str(mid) + "," + str(tid) + "," + str(quant)
                        return redirect(url_for('seats', lst = mlist))
                else:
                    flash("Sorry, there are not enough available tickets.")
            else:
                flash("Please enter a valid number.")

        return render_template('booktype.html', form = form, order = order, mode = mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))



@app.route('/cash/<ls>', methods=['GET','POST'])
def cash(ls):
    if 'key' in session:
        form=CashForm()
        param = ls.split(",")
        lis = ""
        m = Movie.query.filter_by(id = int(param[4])).first()
        app.logger.debug(int(param[4]))

        sts = Seats.query.all()
        det = Seats.query.all()

        for s in sts:
            if not (s.movie_id == int(param[4]) and s.timeslot_id == int(param[5])):
                det.remove(s)
        for d in det:
            if d.status == True and d.reserved == False:
                if d.vip == True:
                    tp = int(param[0])
                    tp += 2
                    param[0] = str(tp)

        #param 0 for vip
        if form.validate_on_submit():
            p = int(form.pay.data)
            if p < int(param[0]):
                flash("Please provide a valid amount.")
                return render_template('cash.html',title='cash payment',form = form, t = int(param[0]), mode = mode, m = m)
            r = p - int(param[0])
            sts = Seats.query.all()
            det = Seats.query.all()
            for s in sts:
                if not (s.movie_id == int(param[4]) and s.timeslot_id == int(param[5])):
                    det.remove(s)
            for d in det:
                if d.status == True and d.reserved == False:
                    lis += str(d.seat)
                    lis += ","
            lis = lis[:-1]
            newl = str(r) + "," + param[1] + "," + param[2] + "," + param[3] + "," + param[4] + "," + param[5] + "," + param[6] + ",card," + param[0]
            t = Tick(ch = int(param[1]), ad = int(param[2]), sr = int(param[3]), movie_id = int(param[4]), timeslot_id = int(param[5]), ptype = "cash", addon = int(param[0]), seats = lis)
            db.session.add(t)
            db.session.commit()
            return redirect(url_for('pay',lst = newl))
        return render_template('cash.html',title='cash payment',form = form, t = int(param[0]), mode = mode, m = m)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))




@app.route('/card/<ls>', methods=['GET','POST'])
def card(ls):
    if 'key' in session:
        form=CashForm()
        param = ls.split(",")
        lis = ""
        m = Movie.query.filter_by(id = int(param[4])).first()
        app.logger.debug(int(param[4]))

        sts = Seats.query.all()
        det = Seats.query.all()
        for s in sts:
            if not (s.movie_id == int(param[4]) and s.timeslot_id == int(param[5])):
                det.remove(s)
        for d in det:
            if d.status == True and d.reserved == False:
                if d.vip == True:
                    tp = int(param[0])
                    tp += 2
                    param[0] = str(tp)


        if form.validate_on_submit():
            p = int(form.pay.data)
            if len(str(p)) != 4:
                flash("PIN should consist of 4 numbers.")
                return render_template('card.html',title='card payment',form = form, t = int(param[0]), mode = mode, m = m)
            r = p - int(param[0])
            sts = Seats.query.all()
            det = Seats.query.all()
            for s in sts:
                if not (s.movie_id == int(param[4]) and s.timeslot_id == int(param[5])):
                    det.remove(s)
            for d in det:
                if d.status == True and d.reserved == False:
                    lis += str(d.seat)
                    lis += ","
            lis = lis[:-1]
            newl = str(r) + "," + param[1] + "," + param[2] + "," + param[3] + "," + param[4] + "," + param[5] + "," + param[6] + ",card," + param[0]
            t = Tick(ch = int(param[1]), ad = int(param[2]), sr = int(param[3]), movie_id = int(param[4]), timeslot_id = int(param[5]), ptype = "card", addon = int(param[0]), seats = lis)
            db.session.add(t)
            db.session.commit()

            return redirect(url_for('pay',lst = newl))
        return render_template('card.html',form = form, t = int(param[0]), mode = mode, m = m)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))




@app.route('/seats/<lst>', methods=['GET','POST'])
def seats(lst):
    if 'key' in session:
        as_val = 0
        param = lst.split(",")

        m = Movie.query.filter_by(id = int(param[4])).first()
        sts = Seats.query.all()
        det = Seats.query.all()
        for s in sts:
            if not (s.movie_id == int(param[4]) and s.timeslot_id == int(param[5])):
                det.remove(s)

        if request.method == 'POST':

            val = request.form.get('val')
            app.logger.debug("val" + str(val))
            if val == "Pay by cash":

                pr = param[0] + "," + param[1] + "," + param[2] + "," + param[3] + "," + param[4] + "," + param[5] + "," + param[6]
                return redirect(url_for('cash', ls = pr))

            elif val == "Pay by card":

                pr = param[0] + "," + param[1] + "," + param[2] + "," + param[3] + "," + param[4] + "," + param[5] + "," + param[6]
                return redirect(url_for('card', ls = pr))

            for s in sts:

                if s.id == int(val):
                    app.logger.debug(s.id)
                    if s.status == False and s.reserved == False:
                        s.status = True
                        db.session.commit()
                    elif s.status == True and s.reserved == False:
                        s.status = False
                        db.session.commit()

            for d in det:
                if (d.status == True and d.reserved == False):
                    as_val += 1

        lft = int(param[1]) + int(param[2]) + int(param[3]) - as_val
        app.logger.debug("lft: " + str(lft))
        return render_template('seats.html', mode=mode, m=m, sts=det, lft=lft)

    else:
        return redirect( url_for( 'login' ))





@app.route('/pay/<lst>', methods=['GET','POST'])
def pay(lst):
    if 'key' in session:
        form = PayForm()
        param = lst.split(",")
        m = Movie.query.filter_by(id = int(param[4])).first()
        order = Order_Ticket.query.filter_by(movie_id=int(param[4]), timeslot_id=int(param[5]), date=func.date(datetime.today())).first()
        app.logger.debug("XXXXXXXXXXXXXXXXXXX: " + str(param[6]))

        sts = Seats.query.all()
        det = Seats.query.all()
        lst = param[1] + "," + param[2] + "," + param[3] + "," + param[4] + "," + param[5] + "," + param[7] + "," + param[8]


        if form.validate_on_submit():
            for s in sts:
                if not (s.movie_id == int(param[4]) and s.timeslot_id == int(param[5])):
                    det.remove(s)
            for d in det:
                if d.status == True and d.reserved == False:
                    lst += ","
                    lst += str(d.seat)
                    app.logger.debug("lst" + str(lst))
                    d.reserved = True
                    db.session.commit()

            return redirect(url_for('pdf_ticket', lis = lst))
        return render_template('pay.html',title='payment', form=form, r = int(param[0]), mode = mode, m = m, pm = param[7])
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))



@app.route('/test', methods=['GET','POST'])
def test():
    return render_template('test.html')


'''
@app.route('/deletets/<id>',methods=['GET','POST'])
def deletets(id):
    t=Timeslot.query.filter_by(id=int(id)).first()
    t.delete = True
    db.session.commit()
    app.logger.debug('Deleted a timeslot')
    return redirect(url_for('TimeSlot'))
'''

@app.route('/pdf/<lis>', methods=['GET','POST'])
def pdf_ticket(lis):
    if 'key' in session:
        mlist = lis.split(",")
        app.logger.debug(lis)
        ss = []
        order = Order_Ticket.query.filter_by(movie_id=int(mlist[3]), timeslot_id=int(mlist[4])).first()
        p1=int(mlist[0]) * 6
        p2=int(mlist[1]) * 8
        p3=int(mlist[2]) * 7
        app.logger.debug(mlist[5])
        vips = int((int(mlist[6]) - (p1 + p2 + p3)) / 2)
        t = int(mlist[6])
        for i in range(7, len(mlist)):
            ss.append(mlist[i])
        html = render_template('ticket.html',title='ticket',c=int(mlist[0]),a=int(mlist[1]),s=int(mlist[2]),t=t,order=order,p1=p1,p2=p2,p3=p3, type = mlist[5], seats = ss, cnt = len(ss), vips=vips)
        return render_pdf(HTML(string=html))
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))


'''
    rendered = render_template('ticket.html')
    pdf = pdfkit.from_string(rendered, False)
    response = make_response(pdf)
    response.headers["Content-Type"]="application/pdf"
    response.headers["Content-Disposition"]="inline; filename = output.pdf"
    return response
'''
@app.route('/tick', methods=['GET','POST'])
def tick():
    if 'key' in session:
        ts = Tick.query.all()
        m = Movie.query.all()
        return render_template('tick.html', mode = mode, ts = ts, m = m)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))


@app.route('/gettick', methods = ['GET', 'POST'])
def gettick():
    if 'key' in session:
        tickid = request.args.get('tickid')
        t = Tick.query.filter_by(id = tickid).first()
        ist = str(t.ch) + "," + str(t.ad) + "," + str(t.sr) + "," + str(t.movie_id) + "," + str(t.timeslot_id) + "," + t.ptype + "," + str(t.addon) + "," + t.seats
        return redirect(url_for('pdf_ticket', lis = ist))
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))


@app.route('/admintools')
def AdminTools():
    if 'key' in session:
        order = Order_Ticket.query.all()
        movie = Movie.query.all()
        for m in movie:
            movie_income = 0
            om = Order_Ticket.query.filter_by(movie_id=m.id)
            for mov in om:
                movie_income = movie_income + mov.income

        return render_template('admin.html',title='Movie statistic', order = order, movie=movie, mode=mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))



@app.route('/chart')
def chart():
    if 'key' in session:
        order=Order_Ticket.query.all()
        movie=Movie.query.all()
        labels=[]
        values=[]

        for o in order:
            d=o.date
            if d not in labels:
                labels.append(d)
                inc=0
                for o in order:
                    if d == o.date:
                        inc=inc+o.income
                values.append(inc)

        return render_template('graph.html',title="graph",labels=labels,values=values, mode=mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))

@app.route('/chart2')
def chart2():
    if 'key' in session:
        order=Order_Ticket.query.all()
        movie=Movie.query.all()
        labels=[]
        values=[]

        for m in movie:
            mv=m.title
            if mv not in labels:
                labels.append(mv)
                inc=0
                for o in order:
                    if m.id == o.movie_id:
                        inc=inc+o.income
                values.append(inc)

        return render_template('graph2.html',title="graph",labels=labels,values=values,mode=mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))

# @app.route('/chart3',methods=['GET','POST'])
# def chart3():
#     if 'key' in session:
#         order=Order_Ticket.query.all()
#         movie=Movie.query.all()
#
#         # labels=[d1,d2]
#         # values=[]
#         # tickets=0
#         # for i in range(2):
#         #     if i==0:
#         #         for o in order:
#         #             if d1==o.date:
#         #                 tickets=tickets+o.quantity
#         #             if d1 not in o.date:
#         #                 tickets=tickets
#         #     if i==1:
#         #         for o in order:
#         #             if d2==o.date:
#         #                 tickets=tickets+o.quantity
#         #             if d2 not in o.date:
#         #                 tickets=tickets
#         #
#         #     values.append(tickets)
#
#         labels=[]
#         values=[]
#
#         for o in order:
#             d=o.date
#             if d not in labels:
#                 labels.append(d)
#                 inc=0
#                 for o in order:
#                     if d == o.date:
#                         inc=inc+o.income
#                 values.append(inc)
#
#         return render_template('graph3.html',title="graph",labels=labels,values=values,mode=mode)
#     else:
#         app.logger.info("Unauthorized access attempt")
#         return redirect( url_for( 'login' ))

@app.route('/chart4',methods=['GET','POST'])
def chart4():
    if 'key' in session:
        order=Order_Ticket.query.all()
        labels1=[]
        values1=[]

        for o in order:
            d=o.date
            if d not in labels1:
                labels1.append(d)
                inc=0
                for o in order:
                    if d == o.date:
                        inc=inc+o.quantity
                values1.append(inc)

        form=DateForm()
        if form.validate_on_submit():
            d1=form.date1.data
            d2=form.date2.data

            order=Order_Ticket.query.filter(Order_Ticket.date <= d2).filter(Order_Ticket.date >= d1).all()
            movie=Movie.query.all()

            labels=[]
            values=[]
            for o in order:
                check=0
                for l in labels:
                    if l == o.date:
                        check = 1
                if check == 0:
                    labels.append(o.date)
            tickets=0
            for l in labels:
                for o in order:
                    if o.date == l:
                        tickets=tickets+o.quantity
                values.append(tickets)
            return render_template('graph3.html',title="graph",labels=labels,values=values,mode=mode, d1=d1, d2=d2)
            # return redirect(url_for("chart3"))
        return render_template("compare.html",labels=labels1,values=values1,form=form,mode=mode)
    else:
        app.logger.info("Unauthorized access attempt")
        return redirect( url_for( 'login' ))


@app.route('/newacc', methods = ['GET', 'POST'])
def newacc():
    form = RegisterForm()
    if form.validate_on_submit():
        md = StaffAccount.query.all()
        for m in md:
            if form.username.data == m.username:
                flash("This username already exists.")
                return render_template('newacc.html', mode=mode, form=form)
        if form.password.data != form.cfpasswd.data:
            flash("The passwords do not match.")
        else:
            id = int(request.args.get('id'))
            if id == 0:
                a = models.StaffAccount(username = form.username.data, password = form.password.data, admin = False)
                db.session.add(a)
                db.session.commit()
            elif id == 1:
                a = models.StaffAccount(username = form.username.data, password = form.password.data, admin = True)
                db.session.add(a)
                db.session.commit()
            flash("New account successfully created")
            return redirect(url_for('login'))

    return render_template('newacc.html', mode=mode, form=form)




def ticketcnt(alpha, beta, gamma, delta):
    if alpha == "" and beta == "" and gamma == "":
        return -1
    breaker = 0
    if alpha != "" and alpha != 0:
        breaker += int(alpha)
    if beta != "" and beta != 0:
        breaker += int(beta)
    if gamma != "" and gamma != 0:
        breaker += int(gamma)
    if (breaker + int(delta)) > 100:
        return -2
    return breaker


def inc(alpha, beta, gamma):
    if (alpha + beta + gamma) < 1:
        return - 1
    total = alpha * 7
    total += beta * 8
    total += gamma * 6
    return total


def ng(alpha, beta, gamma):
    if alpha != None and alpha < 1:
        return 0
    if beta != None and beta < 1:
        return 0
    if gamma != None and gamma < 1:
        return 0
    if alpha == None and beta == None and gamma == None:
        return 0
    return 1
