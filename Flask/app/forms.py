from flask_wtf import FlaskForm, Form
from wtforms import StringField, SubmitField, TextField, StringField, BooleanField, TextAreaField, SelectMultipleField, SelectField, IntegerField, FileField, PasswordField
from wtforms.validators import DataRequired, InputRequired, Optional
from wtforms.fields.html5 import DateField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from .models import Movie, Timeslot, Order_Ticket


class MovieForm(FlaskForm):
    MovieCode = StringField('Movie Code', validators=[DataRequired()])
    title=StringField('Title',validators=[DataRequired()])
    director=StringField('Director',validators=[DataRequired()])
    leadActors=StringField('Lead Actor(s)',validators=[DataRequired()])
    file = FileField()
    description=StringField('Description',validators=[DataRequired()])
    choices = [(g.id, g.slot) for g in Timeslot.query.order_by('id')]
    timeslot =  SelectMultipleField('Select available timeslot(s)', coerce=int, choices = choices)
    submit=SubmitField('Add movie')

class SearchForm(Form):
    target = TextField("target", validators = [Optional()], render_kw = {"placeholder" : "Search for a movie"})

class TimeslotForm(FlaskForm):
    slot = StringField('timeslot', validators=[DataRequired()])
    mm = [(m.id, m.title) for m in Movie.query.order_by('id')]
    movie = SelectMultipleField('movie', coerce=int ,choices = mm)

class LoginForm(Form):
    username = TextField('username', validators = [DataRequired()], render_kw = {"placeholder" : "Username"})
    password = PasswordField('password', validators = [DataRequired()], render_kw = {"placeholder" : "Password"})

class RegisterForm(Form):
    username = TextField('username', validators = [DataRequired()], render_kw = {"placeholder" : "Username"})
    password = PasswordField('password', validators = [DataRequired()], render_kw = {"placeholder" : "Password"})
    cfpasswd = PasswordField('cfpasswd', validators = [DataRequired()], render_kw = {"placeholder" : "Confirm password"})

class TicketForm(FlaskForm):
    ticket_c = IntegerField('Child tickets', validators=[Optional()])
    ticket_a = IntegerField('Aldult tickets', validators=[Optional()])
    ticket_s = IntegerField('Senior tickets', validators=[Optional()])
    submit2=SubmitField('Choose seats')

class CashForm(FlaskForm):
    pay=IntegerField('pay by customer', validators=[DataRequired()])
    submit=SubmitField('Confirm')

class PayForm(FlaskForm):
    submit=SubmitField('Print ticket')

class DateForm(FlaskForm):
    date1=DateField('from',format='%Y-%m-%d')
    date2=DateField('to',format='%Y-%m-%d')
    submit=SubmitField('Search Graph')
