from app import db
from datetime import datetime


arrangement = db.Table('arrangement', db.Model.metadata,
    db.Column('movieid', db.Integer, db.ForeignKey('movie.id')),
    db.Column('timeslotid', db.Integer, db.ForeignKey('timeslot.id'))
)

class Movie(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    MovieCode= db.Column(db.String(200), index=True)
    title = db.Column(db.String(200), index=True)
    director = db.Column(db.String(200), index=True)
    leadActors = db.Column(db.String(200), index=True)
    description = db.Column(db.String(1000), index=True)
    name= db.Column(db.String(200), index=True)
    data = db.Column(db.LargeBinary, index=True)
    timeslot = db.relationship('Timeslot',secondary=arrangement)
    ts = db.relationship("Order_Ticket", back_populates="m")
    t=db.relationship("Seats",back_populates="s")

    def _repr_(self):
        return '<Movie {}>'.format(self.title)

class StaffAccount(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), index=True, unique=True)
    password = db.Column(db.String(200), index=True)
    admin = db.Column(db.Boolean, index=True)

class Timeslot(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    slot = db.Column(db.String(250), index=True)
    delete = db.Column(db.Boolean, default=False)
    movie = db.relationship('Movie',secondary=arrangement)
    m = db.relationship("Order_Ticket", back_populates="ts")
    s=db.relationship("Seats", back_populates="t")

    def __repr__(self):
        return  self.slot

class Order_Ticket(db.Model):
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'), primary_key=True)
    timeslot_id = db.Column(db.Integer, db.ForeignKey('timeslot.id'), primary_key=True)
    date = db.Column(db.Date, index=True, primary_key=True)
    quantity = db.Column(db.Integer, default = 0)
    income = db.Column(db.Integer, default = 0)
    m = db.relationship("Movie", back_populates="ts")
    ts = db.relationship("Timeslot", back_populates="m")

class Tick(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    ch = db.Column(db.Integer, index = True)
    ad = db.Column(db.Integer, index = True)
    sr = db.Column(db.Integer, index = True)
    movie_id = db.Column(db.Integer, index = True)
    timeslot_id = db.Column(db.Integer, index = True)
    ptype = db.Column(db.String(10), index = True)
    addon = db.Column(db.Integer, index = True)
    seats = db.Column(db.String(128), index = True)

class Seats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))
    timeslot_id = db.Column(db.Integer, db.ForeignKey('timeslot.id'))
    seat=db.Column(db.String(120), index=True)
    status=db.Column(db.Boolean, index=True)
    reserved=db.Column(db.Boolean, index=True)
    vip=db.Column(db.Boolean, index=True)
    s= db.relationship("Movie", back_populates="t")
    t= db.relationship("Timeslot", back_populates="s")
